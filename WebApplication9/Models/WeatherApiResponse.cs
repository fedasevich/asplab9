﻿namespace WebApplication9.Models
{
    public class WeatherApiResponse
    {
        public MainInfo main { get; set; }
    }

    public class MainInfo
    {
        public float temp { get; set; }
        public float feels_like { get; set; }
        public float temp_min { get; set; }
        public float temp_max { get; set; }
        public int pressure { get; set; }
        public int humidity { get; set; }
    }
}
