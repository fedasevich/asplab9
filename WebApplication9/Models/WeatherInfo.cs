﻿namespace WebApplication9.Models
{
    public class WeatherInfo
    {
        public float Temperature { get; set; }
        public float FeelsLike { get; set; }
        public float MinTemperature { get; set; }
        public float MaxTemperature { get; set; }
        public int Pressure { get; set; }
        public int Humidity { get; set; }
    }
}
