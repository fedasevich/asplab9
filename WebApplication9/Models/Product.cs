﻿namespace WebApplication9.Models
{
    public class Product
    {
        public int ID { get; set; }
        public string Name { get; set; } = String.Empty;
        public decimal Price { get; set; }
    }
}
