using Microsoft.AspNetCore.DataProtection.KeyManagement;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System;
using System.Net.Http;
using System.Text.Json;
using WebApplication9.Models;


namespace WebApplication9.Pages
{
    public class WeatherModel : PageModel
    {
        [BindProperty]
        public string Region { get; set; }

        public WeatherInfo WeatherInfo { get; private set; }

        public async Task OnPostAsync()
        {
            if (!string.IsNullOrEmpty(Region))
            {
                WeatherInfo = await GetWeather(Region);
            }
        }


        private readonly HttpClient _httpClient;

        public WeatherModel(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        private async Task<WeatherInfo> GetWeather(string region)
        {
            string ApiKey = Environment.GetEnvironmentVariable("API_KEY");
            HttpResponseMessage response = await _httpClient.GetAsync($"https://api.openweathermap.org/data/2.5/weather?q={region}&appid={ApiKey}&units=metric");

            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                var deserializedResponse = JsonSerializer.Deserialize<WeatherApiResponse>(content);

                if (deserializedResponse != null && deserializedResponse.main != null)
                {
 
                    return new WeatherInfo
                    {
                        Temperature = deserializedResponse.main.temp,
                        FeelsLike = deserializedResponse.main.feels_like,
                        MinTemperature = deserializedResponse.main.temp_min,
                        MaxTemperature = deserializedResponse.main.temp_max,
                        Pressure = deserializedResponse.main.pressure,
                        Humidity = deserializedResponse.main.humidity
                    };
                }
            }

            return null; 
        }
    }
    }
