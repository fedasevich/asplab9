using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using WebApplication9.Models;

namespace WebApplication9.Pages
{
    public class ProductsModel : PageModel
    {
        public List<Product> Products { get; set; } = new List<Product>
{
    new Product { ID = 1, Name = "Product 1", Price = 10.99m },
    new Product { ID = 2, Name = "Product 2", Price = 19.99m },
    new Product { ID = 3, Name = "Product 3", Price = 25.50m },
    new Product { ID = 4, Name = "Product 4", Price = 15.75m },
    new Product { ID = 5, Name = "Product 5", Price = 12.00m }
};

        public void OnGet()
        {
        }
    }
}
